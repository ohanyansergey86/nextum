<?php

namespace App\Http\Controllers;

use App\Services\LyricService;
use App\Http\Requests\StoreLyricRequest;
use App\Http\Requests\UpdateLyricRequest;
use Illuminate\Http\RedirectResponse;

class LyricController extends Controller
{
    /**
     * LyricController constructor
     */
    public function __construct(private LyricService $lyricService)
    {
        $this->lyricService = $lyricService;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $lyrics = $this->lyricService->getLyrics();

        return view('lyrics.index', compact('lyrics'));
    }

    /**
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('lyrics.create');
    }

    /**
     * @param StoreLyricRequest $request
     * @return RedirectResponses
     */
    public function store(StoreLyricRequest $request): RedirectResponse
    {
        try {
            $lyric = $this->lyricService->createLyric($request);

            return redirect()
                ->route('lyrics.index')
                ->with('success', 'Lyric successfully added.');
        } catch (Exception $e) {
            return redirect()->back()->withInput();
        }
    }

    /**
     * @param  int $id
     * @return \Illuminate\View\View|\Illuminate\Http\RedirectResponse
     */
    public function show($id)
    {
        $lyric = $this->lyricService->getLyric($id);

        if ($lyric) {
            return view('lyrics.pdf.template', compact('lyric'));
        } else {
            return redirect()
                ->route('lyrics.index')
                ->with('error', 'Lyric not found.');
        }
    }

    /**
     * @param  int $id
     * @return \Illuminate\View\View|\Illuminate\Http\RedirectResponse
     */
    public function edit($id)
    {
        $lyric = $this->lyricService->getLyric($id);

        if ($lyric) {
            return view('lyrics.edit', compact('lyric'));
        } else {
            return redirect()
                ->route('lyrics.index')
                ->with('error', 'Lyric not found.');
        }
    }

    /**
     * @param int $id
     * @param UpdateLyricRequest $request
     * @return RedirectResponse
     */
    public function update($id, UpdateLyricRequest $request): RedirectResponse
    {
        try {
            $result = $this->lyricService->updateLyric($id, $request);

            if($result) {
                return redirect()
                    ->route('lyrics.index')
                    ->with('success', 'Lyric successfully updated.');
            }

            return redirect()->back()->with('error', 'Something went wrong, please try again.');
        } catch (Exception $e) {
            return redirect()->back()->withInput();
        }
    }

    /**
     * @param int $id
     * @return RedirectResponse
     */
    public function destroy($id): RedirectResponse
    {
        try {
            $result = $this->lyricService->deleteLyric($id);

            if ($result) {
                return redirect()
                    ->route('lyrics.index')
                    ->with('success', 'Lyric successfully deleted.');
            }

            return redirect()
                ->route('lyrics.index')
                ->with('error', 'Lyric not found.');
        } catch (Exception $e) {
            return redirect()->back()->with('error', 'Something went wrong, please try again.');
        }
    }
}
