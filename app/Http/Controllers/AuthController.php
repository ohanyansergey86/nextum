<?php

namespace App\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use App\Services\AuthService;
use App\Http\Requests\LoginRequest;
use Exception;

class AuthController extends Controller
{
    /**
     * AuthController constructor
     */
    public function __construct(private AuthService $authService)
    {
        $this->authService = $authService;
    }

    /**
     * @param LoginRequest $request
     * @return RedirectResponse
     */
    public function login(LoginRequest $request): RedirectResponse
    {
        try {
            if ($this->authService->login($request->input('name'), $request->input('password'))) {
                return redirect()->route('dashboard');
            }

            return back()->with('error', 'Incorrect email or password.');
        } catch (Exception $e) {
            return redirect()->back()->withInput();
        }
    }

    /**
     * @return RedirectResponse
     */
    public function logout(): RedirectResponse
    {
        $this->authService->logout();
        return redirect()->route('welcome');
    }

}
