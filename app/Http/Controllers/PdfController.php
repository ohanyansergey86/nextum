<?php

namespace App\Http\Controllers;

use App\Services\LyricService;
use App\Services\PdfService;
use App\Services\ZipService;
use App\Interfaces\GenerateExportInterface;
use Illuminate\Http\RedirectResponse;

class PdfController extends Controller implements GenerateExportInterface
{
    /**
     * PdfController constructor
     */
    public function __construct(
        private LyricService $lyricService,
        private PdfService $pdfService,
        private ZipService $zipService
    )
    {
        $this->lyricService = $lyricService;
        $this->pdfService = $pdfService;
        $this->zipService = $zipService;
    }

    /**
     * @param int $id
     * @return RedirectResponse
     */
    public function generateExport($id): RedirectResponse
    {
        $lyric = $this->lyricService->getLyric($id);

        if ($lyric) {
            $result = $this->pdfService->generate($lyric);

            if($result) {
                $download = $this->zipService->createArchive($lyric->title, $result);

                if($download) {
                    session()->put('fileToDownload', $download);
                    return redirect()->route('download');
                }

                return redirect()->back()->with('error', 'Failed creating ZIP archive.');
            }

            return redirect()->back()->with('error', 'PDF generating failed.');
        } else {
            return redirect()
                ->route('lyrics.index')
                ->with('error', 'Lyric not found.');
        }
    }
}
