<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateLyricRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'title' => ['required', Rule::unique('lyrics')->ignore($this->id, 'id')],
            'part_1' => ['nullable', 'required_without_all:part_2,part_3'],
            'part_2' => ['nullable', 'required_without_all:part_1,part_3'],
            'part_3' => ['nullable', 'required_without_all:part_1,part_2'],
        ];
    }
}
