<?php

namespace App\Services;

use App\Models\Lyric;
use Mpdf\Mpdf;
use Illuminate\Support\Facades\File;
use App\Traits\CreateDirectoryTrait;
use Mpdf\MpdfException;

class PdfService
{
    use CreateDirectoryTrait;

    const PDF_PROPS = [
        'margin_left' => 0,
        'margin_right' => 0,
        'margin_top' => 0,
        'margin_bottom' => 0,
    ];

    /**
     * @var string
     */
    private $publicPath;

    /**
     * PdfService constructor
     */
    public function __construct()
    {
        $this->publicPath = public_path('pdfs');
        $this->createDirectory();
    }

    /**
     * @param Lyric $lyric
     * @return array|bool
     * @throws MpdfException
     */
    public function generate(Lyric $lyric)
    {
        $view = view('lyrics.pdf.template', [
            'lyric' => $lyric,
        ])->render();

        $fileName = time() . '.pdf';
        $pdfFilePath = $this->publicPath . '/' . $fileName;
        $pdf = new Mpdf(self::PDF_PROPS);
        $pdf->WriteHTML($view);
        $pdf->Output($pdfFilePath, 'F');

        if(File::exists($pdfFilePath)) {
            return [
                'filePath' => $pdfFilePath,
                'fileName' => $fileName,
            ];
        }

        return false;
    }
}
