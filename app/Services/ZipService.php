<?php

namespace App\Services;

use Illuminate\Support\Facades\Storage;
use App\Traits\CreateDirectoryTrait;
use Illuminate\Support\Facades\File;
use ZipArchive;

class ZipService
{
    use CreateDirectoryTrait;

    /**
     * @var string
     */
    private $publicPath;

    /**
     * ZipService constructor
     */
    public function __construct()
    {
        $this->publicPath = public_path('archive');
        $this->createDirectory();
        session()->forget('fileToDownload');
    }

    /**
     * @param string $title
     * @param array $result
     * @return bool|string
     */
    public function createArchive($title, $result)
    {
        $zip = new ZipArchive();
        $zipFileName = $title . '_' . time() . '.zip';
        $zipFilePath = $this->publicPath . '/' . $zipFileName;

        if($zip->open($zipFilePath, ZipArchive::CREATE) === true) {
            $zip->addFile($result['filePath'], $result['fileName']);
            $zip->close();

            if(File::exists($zipFilePath)) {
                return $zipFileName;
            }

            return false;
        }

        return false;
    }
}
