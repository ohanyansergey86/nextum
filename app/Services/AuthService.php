<?php

namespace App\Services;

use Illuminate\Support\Facades\Auth;

class AuthService
{
    /**
     * @param $name
     * @param $password
     * @return bool
     */
    public function login($name, $password)
    {
        return Auth::attempt(['name' => $name, 'password' => $password]);
    }

    /**
     * @return void
     */
    public function logout(): void
    {
        Auth::logout();
    }
}
