<?php

namespace App\Services;

use App\Models\Lyric;
use App\Http\Requests\StoreLyricRequest;
use App\Http\Requests\UpdateLyricRequest;

class LyricService
{
    /**
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getLyrics()
    {
        return Lyric::query()->where('user_id', auth()->id())->get();
    }

    /**
     * @param StoreLyricRequest $request
     * @return mixed
     */
    public function createLyric(StoreLyricRequest $request)
    {
        return Lyric::create([
            'user_id' => auth()->id(),
            'title' => $request->input('title'),
            'part_1' => $request->input('part_1'),
            'part_2' => $request->input('part_2'),
            'part_3' => $request->input('part_3'),
        ]);
    }

    /**
     * @param int $id
     * @return Lyric|false
     */
    public function getLyric($id)
    {
        $lyric = Lyric::find($id);

        if ($lyric) {
            return $lyric;
        }

        return false;
    }

    /**
     * @param int $id
     * @param UpdateLyricRequest $request
     * @return bool
     */
    public function updateLyric($id, UpdateLyricRequest $request)
    {
        $lyric = Lyric::find($id);

        if (!$lyric) {
            return false;
        }

        return $lyric->update([
            'title' => $request->input('title'),
            'part_1' => $request->input('part_1'),
            'part_2' => $request->input('part_2'),
            'part_3' => $request->input('part_3'),
        ]);
    }

    /**
     * @param int $id
     * @return bool
     */
    public function deleteLyric($id)
    {
        $lyric = Lyric::find($id);

        if ($lyric) {
            $lyric->delete();
            return true;
        }

        return false;
    }
}
