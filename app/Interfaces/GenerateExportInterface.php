<?php

namespace App\Interfaces;

interface GenerateExportInterface
{
    /**
     * @param $id
     * @return mixed
     */
    public function generateExport($id): mixed;
}
