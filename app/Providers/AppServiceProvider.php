<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\AuthService;
use App\Services\LyricService;
use App\Services\PdfService;
use App\Services\ZipService;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind(AuthService::class, function () {
            return new AuthService();
        });

        $this->app->bind(LyricService::class, function () {
            return new LyricService();
        });

        $this->app->bind(PdfService::class, function () {
            return new PdfService();
        });

        $this->app->bind(ZipService::class, function () {
            return new ZipService();
        });
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
