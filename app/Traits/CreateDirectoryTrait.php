<?php

namespace App\Traits;

use Illuminate\Support\Facades\File;

trait CreateDirectoryTrait
{
    /**
     * @return void
     */
    public function createDirectory(): void
    {
        if (!File::isDirectory($this->publicPath)) {
            File::makeDirectory($this->publicPath, 0777, true);
        }
    }
}
