<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Lyric;
use App\Models\User;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\Log;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Lyric>
 */
class LyricFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        try {
            $user = User::where('email', 'admin@example.com')->first();

            if ($user) {
                return [
                    'user_id' => $user->id,
                    'title' => $this->faker->unique()->word,
                    'part_1' => '<p>' . implode('</p><p>', $this->faker->paragraphs(4)) . '</p>',
                    'part_2' => '<p>' . implode('</p><p>', $this->faker->paragraphs(4)) . '</p>',
                    'part_3' => '<p>' . implode('</p><p>', $this->faker->paragraphs(4)) . '</p>',
                ];
            }
        } catch (\Exception $e) {
            Log::error(__CLASS__ . '::' . __FUNCTION__ . "->" . $e->getMessage());
            return [];
        }
    }
}
