<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class CreateTestUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        try {
            $user = User::query()->where('email', 'admin@example.com')->first();

            if (!$user) {
                User::query()->create([
                    'name' => 'admin',
                    'email' => 'admin@example.com',
                    'password' => Hash::make('12345678'),
                ]);
            }
        } catch (\Exception $e) {
            Log::error(__CLASS__ . '::' . __FUNCTION__ . "->" . $e->getMessage());
        }
    }
}
