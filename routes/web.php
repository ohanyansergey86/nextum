<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\LyricController;
use App\Http\Controllers\PdfController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

Route::get('/login', function () {
    return view('login');
})->name('login');
Route::post('/login', [AuthController::class, 'login'])->name('attempt.login');

Route::middleware('auth')
    ->group(function () {
        Route::get('/dashboard', function () {
            return view('dashboard');
        })->name('dashboard');

        Route::get('/logout', [AuthController::class, 'logout'])->name('logout');

        Route::prefix('lyrics')
            ->as('lyrics.')
            ->group(function () {
                Route::get('', [LyricController::class, 'index'])->name('index');
                Route::get('create', [LyricController::class, 'create'])->name('create');
                Route::post('create', [LyricController::class, 'store'])->name('store');
                Route::get('{id}', [LyricController::class, 'show'])->name('show');
                Route::get('{id}/edit', [LyricController::class, 'edit'])->name('edit');
                Route::put('{id}', [LyricController::class, 'update'])->name('update');
                Route::delete('{id}', [LyricController::class, 'destroy'])->name('destroy');
            });

        Route::post('{id}/pdf-generate', [PdfController::class, 'generateExport'])->name('pdf.generate');

        Route::get('/download', function () {
            return view('download');
        })->name('download');
    });
