@extends('layouts.dashboard')

@section('content')
    <div class="container-fluid mt-5">
        <div>
            <div>
                <h1 class="mb-5">Download</h1>
            </div>
        </div>
        @if(session('fileToDownload'))
            <div>
                <a href="{{ asset('archive/' . session('fileToDownload')) }}" class="btn btn-primary btn-xl" download>
                    Download ZIP
                </a>
            </div>
        @endif
    </div>
@endsection

