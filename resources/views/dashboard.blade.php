@extends('layouts.dashboard')

@section('content')
    <div class="container-fluid mt-5">
        <div class="card card-icon mb-4">
            <div class="row g-0">
                <div class="col-auto card-icon-aside bg-primary">
                    <i class="fa-solid fa-check-double text-white"></i>
                </div>
                <div class="col">
                    <div class="card-body py-5">
                        <h5 class="card-title">Hi {{ auth()->user()->name }}</h5>
                        <p class="card-text">
                            Welcome back...
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
