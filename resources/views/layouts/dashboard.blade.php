<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <title>Nextum</title>
    @include('layouts.styles')
</head>
<body class="nav-fixed">
<nav class="topnav navbar navbar-expand shadow justify-content-between justify-content-sm-start navbar-light bg-white"
     id="sidenavAccordion">
    <!-- Sidenav Toggle Button-->
    <button class="btn btn-icon btn-transparent-dark order-1 order-lg-0 me-2 ms-lg-2 me-lg-0" id="sidebarToggle">
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
             stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
             class="feather feather-menu">
            <line x1="3" y1="12" x2="21" y2="12"></line>
            <line x1="3" y1="6" x2="21" y2="6"></line>
            <line x1="3" y1="18" x2="21" y2="18"></line>
        </svg>
    </button>
    <span class="navbar-brand pe-3 ps-4 ps-lg-2">Nextum</span>
    <!-- Navbar Items-->
    <ul class="navbar-nav align-items-center ms-auto">
        <!-- User Dropdown-->
        <li class="nav-item dropdown no-caret dropdown-user me-3 me-lg-4">
            <a class="btn btn-icon btn-transparent-dark dropdown-toggle"
               id="navbarDropdownUserImage"
               href="javascript:void(0);"
               role="button"
               data-bs-toggle="dropdown"
               aria-haspopup="true"
               aria-expanded="false">
                <img class="img-fluid" src="{{ asset('assets/images/avatar.png') }}">
            </a>
            <div class="dropdown-menu dropdown-menu-end border-0 shadow animated--fade-in-up"
                 aria-labelledby="navbarDropdownUserImage">
                <h6 class="dropdown-header d-flex align-items-center">
                    <img class="dropdown-user-img" src="{{ asset('assets/images/avatar.png') }}">
                    <div class="dropdown-user-details">
                        <div class="dropdown-user-details-name">{{ auth()->user()->email }}</div>
                        <div class="dropdown-user-details-email">{{ auth()->user()->name }}</div>
                    </div>
                </h6>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item justify-content-center" href="{{ route('logout') }}">
                    Logout
                    <div class="dropdown-item-icon p-2">
                        <i class="fa-solid fa-right-from-bracket"></i>
                    </div>
                </a>
            </div>
        </li>
    </ul>
</nav>
<div id="layoutSidenav">
    <div id="layoutSidenav_nav">
        <nav class="sidenav shadow-right sidenav-light">
            <div class="sidenav-menu pt-5">
                <div class="nav accordion" id="accordionSidenav">
                    <a class="nav-link {{ (request()->is('dashboard')) ? 'active' : '' }}"
                       href="{{ route('dashboard') }}"
                    >
                        <div class="nav-link-icon">
                            <span>
                                <i class="fa-solid fa-cube"></i>
                            </span>
                        </div>
                        Dashboard
                    </a>
                    <a class="nav-link {{ (request()->is('lyrics')) ? 'active' : '' }}"
                       href="{{ route('lyrics.index') }}"
                    >
                        <div class="nav-link-icon">
                            <span>
                                <i class="fa-solid fa-list"></i>
                            </span>
                        </div>
                        My Lyrics
                    </a>
                    <a class="nav-link {{ (request()->is('lyrics/create')) ? 'active' : '' }}"
                       href="{{ route('lyrics.create') }}"
                    >
                        <div class="nav-link-icon">
                            <span>
                                <i class="fa-solid fa-plus"></i>
                            </span>
                        </div>
                        Add new Lyric
                    </a>
                </div>
            </div>
        </nav>
    </div>
    <div id="layoutSidenav_content">
        @yield('content')
    </div>
</div>
@include('layouts.scripts')
</body>
</html>
