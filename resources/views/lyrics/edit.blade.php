@extends('layouts.dashboard')

@section('content')
    <div class="container-fluid mt-5">
        <div>
            <div>
                <h1 class="mb-5">Edit Lyric</h1>
            </div>
        </div>
        <form
            method="post"
            action="{{ route('lyrics.update', $lyric->id) }}"
            class="container-fluid border-start border-primary p-3"
        >
            @csrf
            @method('PUT')
            <div class="row">
                <div class="col-12">
                    <div class="mb-3">
                        <label for="title"><b>Title</b></label>
                        <input
                            class="form-control"
                            id="title"
                            type="text"
                            placeholder="Title"
                            name="title"
                            value="{{ old('title', $lyric->title) }}"
                        >
                        @error('title')
                            <span class="text-xs text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="col-md-4">
                    <label for="part_1"><b>First part</b></label>
                    <textarea name="part_1" id="part_1">{{ old('part_1', $lyric->part_1) }}</textarea>
                    @error('part_1')
                        <span class="text-xs text-danger">{{ $message }}</span>
                    @enderror
                </div>
                <div class="col-md-4">
                    <label for="part_2"><b>Second part</b></label>
                    <textarea name="part_2" id="part_2">{{ old('part_2', $lyric->part_2) }}</textarea>
                    @error('part_2')
                        <span class="text-xs text-danger">{{ $message }}</span>
                    @enderror
                </div>
                <div class="col-md-4">
                    <label for="part_3"><b>Third part</b></label>
                    <textarea name="part_3" id="part_3">{{ old('part_3', $lyric->part_3) }}</textarea>
                    @error('part_3')
                        <span class="text-xs text-danger">{{ $message }}</span>
                    @enderror
                </div>
                <div class="col-12">
                    <button type="submit" class="btn btn-primary mt-3">Update</button>
                </div>
            </div>
        </form>
    </div>
@endsection
