@extends('layouts.dashboard')

@section('content')
    <div class="container-fluid mt-5">
        <div>
            <div>
                <h1 class="mb-5">My Lyrics</h1>
                @include('layouts.notifications')
            </div>
        </div>
        <table class="table table-bordered bg-white">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Title</th>
                    <th scope="col">Created</th>
                    <th scope="col">Last update</th>
                    <th scope="col">Actions</th>
                    <th scope="col">Export</th>
                </tr>
            </thead>
            <tbody>
                @forelse($lyrics as $key => $lyric)
                    <tr>
                        <td><b>{{ $key + 1 }}</b></td>
                        <td>{{ $lyric->title }}</td>
                        <td>{{ $lyric->created_at }}</td>
                        <td>{{ $lyric->updated_at }}</td>
                        <td>
                            <a href="{{ route('lyrics.show', $lyric->id) }}" target="_blank" class="btn btn-success btn-xs">
                                Preview
                            </a>
                            <a href="{{ route('lyrics.edit', $lyric->id) }}" class="btn btn-primary btn-xs">
                                Edit
                            </a>
                            <form action="{{ route('lyrics.destroy', $lyric->id) }}" class="d-inline-block" method="post">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger btn-xs">
                                    Delete
                                </button>
                            </form>
                        </td>
                        <td>
                            <form action="{{ route('pdf.generate', $lyric->id) }}" method="post">
                                @csrf
                                <button type="submit" class="btn btn-yellow btn-xs">
                                    Generate PDF & export ZIP
                                </button>
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="12" class="text-center">
                            No lyrics found.
                        </td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
@endsection
