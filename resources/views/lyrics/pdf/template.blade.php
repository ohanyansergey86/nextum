<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>Title</title>
    @include('lyrics.pdf.styles')
</head>
<body>
    <div class="wrap">
        <div>
            <h1>{{ $lyric->title }}</h1>
        </div>
        <div>
            {!! $lyric->part_1 !!}
            <br>
            {!! $lyric->part_2 !!}
            <br>
            {!! $lyric->part_3 !!}
        </div>
    </div>
</body>
</html>
