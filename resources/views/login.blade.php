@extends('layouts.layout')

@section('content')
    <div class="container pt-15">
        <div class="row">
            <div class="col-md-4 offset-md-4">
                <div class="card shadow-lg border-0 rounded-lg">
                    <div class="card-header justify-content-center">
                        <h3 class="fw-light my-4">Login</h3>
                        @if(session('error'))
                            <span class="text-xs text-danger">{{ session('error') }}</span>
                        @endif
                    </div>
                    <div class="card-body">
                        <!-- Login form-->
                        <form method="post" action="{{ route('attempt.login') }}">
                            @csrf
                            <div class="mb-3">
                                <label class="small mb-1" for="username">Username</label>
                                <input class="form-control" name="name" id="username" type="text" placeholder="Username">
                                @error('name')
                                    <span class="text-xs text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="mb-3">
                                <label class="small mb-1" for="password">Password</label>
                                <input class="form-control" name="password" id="password" type="password" placeholder="Password">
                                @error('password')
                                    <span class="text-xs text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="d-flex align-items-center justify-content-between mt-4 mb-0">
                                <button type="submit" class="btn btn-primary">Login</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
