@extends('layouts.layout')

@section('content')
    <div class="container pt-15">
        <div class="row">
            <div class="col-md-4 offset-md-4">
                <div class="card text-center border-0">
                    <div class="card-header bg-transparent justify-content-center py-4"><h5 class="text-primary mb-0">NEXTUM</h5></div>
                    <div class="card-body p-5">
                        <p class="lead mb-4">Test task for Nextum</p>
                    </div>
                    <a href="{{ route('login') }}" class="card-footer d-flex align-items-center justify-content-center">
                        Login
                        <i class="fa-solid fa-arrow-right-long m-2"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection
